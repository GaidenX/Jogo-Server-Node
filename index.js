var express = require('express');
var app =  express();
var server = require('http').createServer(app);
var porta = process.env.PORT || 3000;
var jsonfile = require("jsonfile");
var bodyParser = require('body-parser');
var fs = require("fs")

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.use(express.static(__dirname + '/public'))

// ------ Server logic --------
var file;
var numTurn = 1;
var playerScreen1 = {
	jogador: "Jogador 1",
	flag: false,
	rankExist: false,
	turn: false 
};
var playerScreen2 = {
	jogador: "Jogador 2",
	flag: false,
	rankExist: false,
	turn: false 
};
var userListSel;
var systemListSel = [];

var obj = {
   table: []
};

var pontoAleatorio;

// escrita
app.post('/pontuacao', function (req, res) {
 	 var userIdFB = req.body.IdUserFB;
fs.readFile('public/data/config.json', 'utf8', function readFileCallback(err, data){
    if (err){
        console.log(err);
    } else {
    pontoAleatorio = Math.floor((Math.random() * 100) + 1);
    obj = JSON.parse(data); //now it an object
    obj.table.push({id: userIdFB, pontos: pontoAleatorio}); //add some data
    json = JSON.stringify(obj); //convert it back to json
    //fs.writeFile('public/data/config.json', json, 'utf8'); // write it back 
	}
	});
});

app.get('/initPartida', function (req, res) {
	 "use strict";
	if (playerScreen1.flag == true){
  		playerScreen2.flag = true;
  		playerScreen2.turn = false;
  		res.send(playerScreen2);
  	}
	else if (playerScreen1.flag == false) {
		playerScreen1.flag = true;
		playerScreen1.turn = true;
  		res.send(playerScreen1);
  	}
});

app.post('/itensSelecionados1', function (req, res) {
	if(playerScreen1.turn == true){
	 userListSel = req.body.itemSeletec;
	 	systemListSel.push(userListSel);
	 		console.log(systemListSel);
	 			playerScreen1.turn = false;
	 				playerScreen2.turn = true;
	 					numTurn++;
	 }
});

app.post('/itensSelecionados2', function (req, res) {
	if(playerScreen2.turn == true){
	 userListSel = req.body.itemSeletec;
	 	systemListSel.push(userListSel);
	 		console.log(systemListSel);
	 			playerScreen1.turn = true;
	 				playerScreen2.turn = false;
	 					numTurn++;
	 }
});

app.get('/statusGame', function (req, res) {	
	var status = {
		"playerStatus1" : playerScreen1,
		"playerStatus2" : playerScreen2,
		"listItensArray" : systemListSel,
		"numTurn": numTurn
	}
  	res.send(status);
});

app.get('/GetitensSelecionados', function (req, res) {
  res.send(userListSel)
});
 
app.listen(porta, function(){
  console.log("server on in: " + porta)
});

/*
app.listen(3000, function(){
  console.log("server on")
});

Talvez depois tenha que ser por socket.io

var server = http.createServer(function(req, res) {
  res.writeHead(200);
  res.end('Aqui vai a chamada inicial');
});

server.listen(3000);
*/
